SET PYTHONPATH=%~dp0;%~dp0\fm_to_estry
SET PROJ_LIB=proj_data


pyinstaller -y --workpath ./buildlog --distpath ./release/windows/x64 --add-data data/fm_units.json;data --add-data data/HELP;data --add-data %PROJ_LIB%;osgeo/data/proj --add-data ./*.txt;./ --add-data ./fm_to_estry/parsers/units/*.py;./parsers/units --add-data ./fm_to_estry/converters/*.py;./converters --additional-hooks-dir=./hooks/. -p ./fm_to_estry -p ./fm_to_estry/parsers -p ./fm_to_estry/parsers/units -p ./fm_to_estry/converters --name fm_to_estry ./fm_to_estry/main.py

