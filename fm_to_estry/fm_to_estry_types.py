import typing
from pathlib import Path


PathLike = typing.Union[str, bytes, Path]
